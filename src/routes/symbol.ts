import {Controller, Param, Body, Get, Post, Put, Delete} from "routing-controllers";
import {getRepository} from "typeorm";
import {Symbol} from "../domain/symbol/symbol.entity";

@Controller('/symbols')
export class SymbolController {
    get repository() {
        return getRepository(Symbol);
    }

    @Get("/")
    getAll() {
       return this.repository.find();
    }

    @Get("/:id")
    getOne(@Param("id") id: number) {
       return this.repository.findOneById(id);
    }

    @Post("/")
    post(@Body() symbol: any) {
       return this.repository.save(symbol);
    }

    @Put("/:id")
    put(@Param("id") id: number, @Body() symbol: any) {
       return this.repository.updateById(id, symbol);
    }

    @Delete("/:id")
    remove(@Param("id") id: number) {
       return this.repository.deleteById(id);
    }

}