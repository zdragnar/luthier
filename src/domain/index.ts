import { ConnectionOptions, createConnection, Connection } from 'typeorm';

const extension = process.env.NODE_ENV === 'development' ? 'ts' : 'js';

let connectionAttemptCount = 0;

import { Symbol as SymbolEntity } from './symbol/symbol.entity';

export async function setupTypeORM(connectionConfig: Partial<ConnectionOptions>): Promise<Connection> {
  connectionAttemptCount++;

  try {
    return await createConnection({
      type: "postgres",
      logging: true,
      entities: [
        SymbolEntity,
      ],
      ...connectionConfig
    } as ConnectionOptions);

  } catch(e) {
    if (connectionAttemptCount === 5) {
      console.error('Unable to establish database connection, quitting');
      process.exitCode = 1;
      process.exit();
    }
    console.error(e);
    console.log(`Connection attempt ${connectionAttemptCount} failed, retrying in 2 seconds`);
    setTimeout(() => {
      return setupTypeORM(connectionConfig);
    }, 2000);
  }
}
