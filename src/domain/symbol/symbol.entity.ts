import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Symbol {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
}