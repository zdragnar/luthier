import "reflect-metadata";

import { createExpressServer } from 'routing-controllers';
// this example uses express web framework so we know what longer build times
// do and how Dockerfile layer ordering matters. If you mess up Dockerfile ordering
// you'll see long build times on every code change + build. If done correctly,
// code changes should be only a few seconds to build locally due to build cache.

import * as morgan from 'morgan';
// morgan provides easy logging for express, and by default it logs to stdout
// which is a best practice in Docker. Friends don't let friends code their apps to
// do app logging to files in containers.

import * as bp from 'body-parser';
import * as redis from 'redis';

import * as bluebird from 'bluebird';

import { setupTypeORM } from './domain/index';
import { SymbolController } from './routes/symbol';

// Constants
const PORT = process.env.PORT || 8080;
const REDIS_URL = process.env.REDIS_URL;
const DATABASE_URL = process.env.DATABASE_URL;
const POSTGRES_PASSWORD = process.env.POSTGRES_PASSWORD;
const POSTGRES_USERNAME = process.env.POSTGRES_USERNAME;
const POSTGRES_DB = process.env.POSTGRES_DB;

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const redisClient = redis.createClient({ url: REDIS_URL });

redisClient.on("error", (err) => {
  console.error("REDIS Error " + err);
});

let dbClient;
setupTypeORM({
  url: DATABASE_URL,
  password: POSTGRES_PASSWORD,
  username: POSTGRES_USERNAME
}).then(connection => dbClient = connection);

// if you're not using docker-compose for local development, this will default to 8080
// to prevent non-root permission problems with 80. Dockerfile is set to make this 80
// because containers don't have that issue :)

// Appi
const app = createExpressServer({
  controllers: [SymbolController] // we specify controllers we want to use
 });

app.use(morgan('common'));
app.use(bp.json());

app.get('/health-check', function (req, res) {
	// do app logic here to determine if app is truly healthy
	// you should return 200 if healthy, and anything else will fail
	// if you want, you should be able to restrict this to localhost (include ipv4 and ipv6)
  res.send('200\n');
});

const server = app.listen(PORT, function () {
  console.log('Webserver is ready on internal port ' + PORT);
});


//
// need this in docker container to properly exit since node doesn't handle SIGINT/SIGTERM
// this also won't work on using npm start since:
// https://github.com/npm/npm/issues/4603
// https://github.com/npm/npm/pull/10868
// https://github.com/RisingStack/kubernetes-graceful-shutdown-example/blob/master/src/index.js
// if you want to use npm then start with `docker run --init` to help, but I still don't think it's
// a graceful shutdown of node process
//

// quit on ctrl-c when running docker in terminal
process.on('SIGINT', function onSigint () {
	console.info('Got SIGINT. Graceful shutdown ', new Date().toISOString());
  shutdown();
});

// quit properly on docker stop
process.on('SIGTERM', function onSigterm () {
  console.info('Got SIGTERM. Graceful shutdown ', new Date().toISOString());
  shutdown();
})

// shut down server
function shutdown() {
  server.close(function onServerClosed (err) {
    if (err) {
      console.error(err);
      process.exitCode = 1;
		}
		process.exit();
  })
}
//
// need above in docker container to properly exit
//
